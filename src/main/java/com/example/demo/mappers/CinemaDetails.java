package com.example.demo.mappers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CinemaDetails {

    private String numberOfSeats;

    private String id;

    private String technology;

    private String hallName;

}
