package com.example.demo.mappers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CinemaReqDto {
    private String before;
    private String after;
    private int first;
    private int last;
}
