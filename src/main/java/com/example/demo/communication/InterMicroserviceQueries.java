package com.example.demo.communication;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author KinjeMundi
 * @version 1.0.0
 */
@Component
public class InterMicroserviceQueries {

    public String allCinemaDetails(String before, String after,int first,int last) {

        String query = "query{\n" +
                "  allCinemaDetails(before:\""+before+"\",after:\""+after+"\",first:\""+first+"\",last:\""+last+"\"){\n" +
                "    edges{\n" +
                "      node{\n" +
                "        id\n" +
                "        hallName\n" +
                "        numberOfSeats\n" +
                "        technology\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
//        GraphApiRequest request = new GraphApiRequest();
//        request.setQuery(query);
//        request.addArgument("before",before);
//        request.addArgument("after",after);
//        request.addIntegerArgument("first",first);
        return query;
    }

}
