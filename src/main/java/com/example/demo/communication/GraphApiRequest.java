package com.example.demo.communication;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class GraphApiRequest {

    private String query;

    private HashMap<String,Object> variables = new HashMap<>();

    public void addArgument(String variable,String value){
        variables.put(variable,value);
    }

    public void  addIntegerArgument(String variable,int value){
        variables.put(variable,value);
    }

    public void addMapVariable(String name, Map value) {
        variables.put(name, value);
    }
}
