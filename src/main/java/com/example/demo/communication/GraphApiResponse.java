
package com.example.demo.communication;

import lombok.*;

/**
 * @author gothomis-development team
 * @date Dec 02, 2019
 * @version 1.0.0
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class GraphApiResponse {

	private InterServiceInfo data;

	private Object errors;

}
