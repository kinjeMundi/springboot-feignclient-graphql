package com.example.demo.communication;

import com.example.demo.mappers.CinemaDetails;
import com.example.demo.mappers.Response;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * @author KinjeMundi
 * @version 1.0.0
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InterServiceInfo {
    String allCinemaDetails;
}
