package com.example.demo.controller;

import com.example.demo.communication.GraphApiResponse;
import com.example.demo.communication.InterMicroserviceQueries;
import com.example.demo.connectors.ProxyExchangeInterface;
import com.example.demo.mappers.CinemaDetails;
import com.example.demo.mappers.CinemaReqDto;
import com.example.demo.mappers.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@GraphQLApi
@Service
public class CinemaDetailsController {

    @Autowired
    ProxyExchangeInterface proxyExchangeInterface;

    @Autowired
    InterMicroserviceQueries interMicroserviceQueries;

    @GraphQLQuery(name = "allCinemaDetails")
    public Response<CinemaDetails> allCinemaDetails(@GraphQLArgument(name = "before")String before,
                                   @GraphQLArgument(name = "after") String after,
                                   @GraphQLArgument(name = "first") int first,
                                   @GraphQLArgument(name = "last") int last){
        GraphApiResponse apiResponse = proxyExchangeInterface.connect(interMicroserviceQueries.allCinemaDetails("","after",1,5));
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("Receive data : "+apiResponse.getData().getAllCinemaDetails());


        return null;
    }
}
