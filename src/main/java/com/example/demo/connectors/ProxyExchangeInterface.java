package com.example.demo.connectors;

import com.example.demo.communication.GraphApiRequest;
import com.example.demo.communication.GraphApiResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "https://etmdb.com",url = "https://etmdb.com")
public interface ProxyExchangeInterface {

    @PostMapping(value = "/graphql", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public GraphApiResponse connect(@RequestBody String query);

}
